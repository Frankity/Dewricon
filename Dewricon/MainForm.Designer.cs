﻿namespace Dewricon
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tCtrl = new System.Windows.Forms.TabControl();
            this.tP_Players = new System.Windows.Forms.TabPage();
            this.lV_PlayersBlue = new System.Windows.Forms.ListView();
            this.colH_BluePlayerUniqueID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colH_BluePlayerIPAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colH_BluePlayerName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colH_BluePlayerTeam = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colH_BluePlayerScore = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colH_BluePLayerKills = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colH_BluePlayerDeaths = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colH_BluePlayerAssists = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cMS_Player = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tMSMI_KickPlayer = new System.Windows.Forms.ToolStripMenuItem();
            this.tMSMI_BanPlayer = new System.Windows.Forms.ToolStripMenuItem();
            this.nUD_RefreshInterval = new System.Windows.Forms.NumericUpDown();
            this.lb_Refresh = new System.Windows.Forms.Label();
            this.grB_ServerInfo = new System.Windows.Forms.GroupBox();
            this.tB_MaxPlayers = new System.Windows.Forms.TextBox();
            this.tB_NumPlayers = new System.Windows.Forms.TextBox();
            this.tB_GameType = new System.Windows.Forms.TextBox();
            this.tB_MapName = new System.Windows.Forms.TextBox();
            this.tB_VoIPEnabled = new System.Windows.Forms.TextBox();
            this.tB_HostName = new System.Windows.Forms.TextBox();
            this.lb_MaxPlayers = new System.Windows.Forms.Label();
            this.lb_NumPlayers = new System.Windows.Forms.Label();
            this.lb_GameType = new System.Windows.Forms.Label();
            this.lb_MapName = new System.Windows.Forms.Label();
            this.lb_VoIPEnabled = new System.Windows.Forms.Label();
            this.lb_HostName = new System.Windows.Forms.Label();
            this.grB_Settings = new System.Windows.Forms.GroupBox();
            this.comB_ServerList = new System.Windows.Forms.ComboBox();
            this.btn_ResetSettings = new System.Windows.Forms.Button();
            this.btn_LoadSettings = new System.Windows.Forms.Button();
            this.btn_SaveSettings = new System.Windows.Forms.Button();
            this.tB_ServerProtocol = new System.Windows.Forms.TextBox();
            this.tB_ServerPort = new System.Windows.Forms.TextBox();
            this.tB_ServerAddress = new System.Windows.Forms.TextBox();
            this.lb_ServerProtocol = new System.Windows.Forms.Label();
            this.lb_ServerPort = new System.Windows.Forms.Label();
            this.lb_SelectServer = new System.Windows.Forms.Label();
            this.lV_PlayersRed = new System.Windows.Forms.ListView();
            this.colH_RedPlayerUniqueID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colH_RedPlayerIPAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colH_RedPlayerName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colH_RedPlayerTeam = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colH_RedPlayerScore = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colH_RedPlayerKills = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colH_RedPlayerDeaths = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colH_RedPlayerAssists = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_QuickSettings = new System.Windows.Forms.Button();
            this.btn_Disconnect = new System.Windows.Forms.Button();
            this.btn_Refresh = new System.Windows.Forms.Button();
            this.btn_Connect = new System.Windows.Forms.Button();
            this.tP_Console = new System.Windows.Forms.TabPage();
            this.btn_ChangeTime = new System.Windows.Forms.Button();
            this.rTB_HiddenOutput = new System.Windows.Forms.RichTextBox();
            this.btn_ConsoleSend = new System.Windows.Forms.Button();
            this.tB_ConsoleInput = new System.Windows.Forms.TextBox();
            this.rTB_ConsoleOutput = new System.Windows.Forms.RichTextBox();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.stS_First = new System.Windows.Forms.StatusStrip();
            this.tSSL_First = new System.Windows.Forms.ToolStripStatusLabel();
            this.bgW_GetPlayerList = new System.ComponentModel.BackgroundWorker();
            this.bgW_GetServerInfo = new System.ComponentModel.BackgroundWorker();
            this.tCtrl.SuspendLayout();
            this.tP_Players.SuspendLayout();
            this.cMS_Player.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_RefreshInterval)).BeginInit();
            this.grB_ServerInfo.SuspendLayout();
            this.grB_Settings.SuspendLayout();
            this.tP_Console.SuspendLayout();
            this.stS_First.SuspendLayout();
            this.SuspendLayout();
            // 
            // tCtrl
            // 
            this.tCtrl.Controls.Add(this.tP_Players);
            this.tCtrl.Controls.Add(this.tP_Console);
            this.tCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tCtrl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tCtrl.Location = new System.Drawing.Point(0, 0);
            this.tCtrl.Name = "tCtrl";
            this.tCtrl.SelectedIndex = 0;
            this.tCtrl.Size = new System.Drawing.Size(866, 502);
            this.tCtrl.TabIndex = 6;
            // 
            // tP_Players
            // 
            this.tP_Players.Controls.Add(this.lV_PlayersBlue);
            this.tP_Players.Controls.Add(this.nUD_RefreshInterval);
            this.tP_Players.Controls.Add(this.lb_Refresh);
            this.tP_Players.Controls.Add(this.grB_ServerInfo);
            this.tP_Players.Controls.Add(this.grB_Settings);
            this.tP_Players.Controls.Add(this.lV_PlayersRed);
            this.tP_Players.Controls.Add(this.btn_QuickSettings);
            this.tP_Players.Controls.Add(this.btn_Disconnect);
            this.tP_Players.Controls.Add(this.btn_Refresh);
            this.tP_Players.Controls.Add(this.btn_Connect);
            this.tP_Players.Location = new System.Drawing.Point(4, 22);
            this.tP_Players.Name = "tP_Players";
            this.tP_Players.Padding = new System.Windows.Forms.Padding(3);
            this.tP_Players.Size = new System.Drawing.Size(858, 476);
            this.tP_Players.TabIndex = 0;
            this.tP_Players.Text = "Players";
            this.tP_Players.UseVisualStyleBackColor = true;
            // 
            // lV_PlayersBlue
            // 
            this.lV_PlayersBlue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lV_PlayersBlue.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colH_BluePlayerUniqueID,
            this.colH_BluePlayerIPAddress,
            this.colH_BluePlayerName,
            this.colH_BluePlayerTeam,
            this.colH_BluePlayerScore,
            this.colH_BluePLayerKills,
            this.colH_BluePlayerDeaths,
            this.colH_BluePlayerAssists});
            this.lV_PlayersBlue.ContextMenuStrip = this.cMS_Player;
            this.lV_PlayersBlue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lV_PlayersBlue.FullRowSelect = true;
            this.lV_PlayersBlue.Location = new System.Drawing.Point(268, 231);
            this.lV_PlayersBlue.Name = "lV_PlayersBlue";
            this.lV_PlayersBlue.Size = new System.Drawing.Size(582, 219);
            this.lV_PlayersBlue.TabIndex = 18;
            this.lV_PlayersBlue.UseCompatibleStateImageBehavior = false;
            this.lV_PlayersBlue.View = System.Windows.Forms.View.Details;
            // 
            // colH_BluePlayerUniqueID
            // 
            this.colH_BluePlayerUniqueID.Tag = "120";
            this.colH_BluePlayerUniqueID.Text = "Unique ID";
            this.colH_BluePlayerUniqueID.Width = 120;
            // 
            // colH_BluePlayerIPAddress
            // 
            this.colH_BluePlayerIPAddress.Tag = "97";
            this.colH_BluePlayerIPAddress.Text = "IP Address";
            this.colH_BluePlayerIPAddress.Width = 97;
            // 
            // colH_BluePlayerName
            // 
            this.colH_BluePlayerName.Tag = "115";
            this.colH_BluePlayerName.Text = "Name";
            this.colH_BluePlayerName.Width = 115;
            // 
            // colH_BluePlayerTeam
            // 
            this.colH_BluePlayerTeam.Tag = "45";
            this.colH_BluePlayerTeam.Text = "Team";
            this.colH_BluePlayerTeam.Width = 54;
            // 
            // colH_BluePlayerScore
            // 
            this.colH_BluePlayerScore.Tag = "45";
            this.colH_BluePlayerScore.Text = "Score";
            this.colH_BluePlayerScore.Width = 45;
            // 
            // colH_BluePLayerKills
            // 
            this.colH_BluePLayerKills.Tag = "45";
            this.colH_BluePLayerKills.Text = "Kills";
            this.colH_BluePLayerKills.Width = 45;
            // 
            // colH_BluePlayerDeaths
            // 
            this.colH_BluePlayerDeaths.Tag = "50";
            this.colH_BluePlayerDeaths.Text = "Deaths";
            this.colH_BluePlayerDeaths.Width = 50;
            // 
            // colH_BluePlayerAssists
            // 
            this.colH_BluePlayerAssists.Tag = "50";
            this.colH_BluePlayerAssists.Text = "Assists";
            this.colH_BluePlayerAssists.Width = 50;
            // 
            // cMS_Player
            // 
            this.cMS_Player.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tMSMI_KickPlayer,
            this.tMSMI_BanPlayer});
            this.cMS_Player.Name = "contextMenuStrip1";
            this.cMS_Player.Size = new System.Drawing.Size(132, 48);
            // 
            // tMSMI_KickPlayer
            // 
            this.tMSMI_KickPlayer.Name = "tMSMI_KickPlayer";
            this.tMSMI_KickPlayer.Size = new System.Drawing.Size(131, 22);
            this.tMSMI_KickPlayer.Text = "Kick Player";
            this.tMSMI_KickPlayer.Click += new System.EventHandler(this.tMSMI_KickPlayer_Click);
            // 
            // tMSMI_BanPlayer
            // 
            this.tMSMI_BanPlayer.Name = "tMSMI_BanPlayer";
            this.tMSMI_BanPlayer.Size = new System.Drawing.Size(131, 22);
            this.tMSMI_BanPlayer.Text = "Ban Player";
            // 
            // nUD_RefreshInterval
            // 
            this.nUD_RefreshInterval.Location = new System.Drawing.Point(101, 429);
            this.nUD_RefreshInterval.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.nUD_RefreshInterval.Name = "nUD_RefreshInterval";
            this.nUD_RefreshInterval.Size = new System.Drawing.Size(40, 22);
            this.nUD_RefreshInterval.TabIndex = 15;
            this.nUD_RefreshInterval.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nUD_RefreshInterval.ValueChanged += new System.EventHandler(this.nUD_RefreshInterval_ValueChanged);
            // 
            // lb_Refresh
            // 
            this.lb_Refresh.AutoSize = true;
            this.lb_Refresh.Location = new System.Drawing.Point(8, 433);
            this.lb_Refresh.Name = "lb_Refresh";
            this.lb_Refresh.Size = new System.Drawing.Size(87, 13);
            this.lb_Refresh.TabIndex = 16;
            this.lb_Refresh.Text = "Refresh Interval";
            // 
            // grB_ServerInfo
            // 
            this.grB_ServerInfo.Controls.Add(this.tB_MaxPlayers);
            this.grB_ServerInfo.Controls.Add(this.tB_NumPlayers);
            this.grB_ServerInfo.Controls.Add(this.tB_GameType);
            this.grB_ServerInfo.Controls.Add(this.tB_MapName);
            this.grB_ServerInfo.Controls.Add(this.tB_VoIPEnabled);
            this.grB_ServerInfo.Controls.Add(this.tB_HostName);
            this.grB_ServerInfo.Controls.Add(this.lb_MaxPlayers);
            this.grB_ServerInfo.Controls.Add(this.lb_NumPlayers);
            this.grB_ServerInfo.Controls.Add(this.lb_GameType);
            this.grB_ServerInfo.Controls.Add(this.lb_MapName);
            this.grB_ServerInfo.Controls.Add(this.lb_VoIPEnabled);
            this.grB_ServerInfo.Controls.Add(this.lb_HostName);
            this.grB_ServerInfo.Location = new System.Drawing.Point(8, 0);
            this.grB_ServerInfo.Name = "grB_ServerInfo";
            this.grB_ServerInfo.Size = new System.Drawing.Size(254, 188);
            this.grB_ServerInfo.TabIndex = 11;
            this.grB_ServerInfo.TabStop = false;
            this.grB_ServerInfo.Text = "Server Info";
            // 
            // tB_MaxPlayers
            // 
            this.tB_MaxPlayers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tB_MaxPlayers.Location = new System.Drawing.Point(212, 134);
            this.tB_MaxPlayers.Name = "tB_MaxPlayers";
            this.tB_MaxPlayers.ReadOnly = true;
            this.tB_MaxPlayers.Size = new System.Drawing.Size(36, 22);
            this.tB_MaxPlayers.TabIndex = 42;
            // 
            // tB_NumPlayers
            // 
            this.tB_NumPlayers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tB_NumPlayers.Location = new System.Drawing.Point(92, 134);
            this.tB_NumPlayers.Name = "tB_NumPlayers";
            this.tB_NumPlayers.ReadOnly = true;
            this.tB_NumPlayers.Size = new System.Drawing.Size(36, 22);
            this.tB_NumPlayers.TabIndex = 41;
            // 
            // tB_GameType
            // 
            this.tB_GameType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tB_GameType.Location = new System.Drawing.Point(92, 106);
            this.tB_GameType.Name = "tB_GameType";
            this.tB_GameType.ReadOnly = true;
            this.tB_GameType.Size = new System.Drawing.Size(156, 22);
            this.tB_GameType.TabIndex = 40;
            // 
            // tB_MapName
            // 
            this.tB_MapName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tB_MapName.Location = new System.Drawing.Point(92, 78);
            this.tB_MapName.Name = "tB_MapName";
            this.tB_MapName.ReadOnly = true;
            this.tB_MapName.Size = new System.Drawing.Size(156, 22);
            this.tB_MapName.TabIndex = 39;
            // 
            // tB_VoIPEnabled
            // 
            this.tB_VoIPEnabled.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tB_VoIPEnabled.Location = new System.Drawing.Point(92, 50);
            this.tB_VoIPEnabled.Name = "tB_VoIPEnabled";
            this.tB_VoIPEnabled.ReadOnly = true;
            this.tB_VoIPEnabled.Size = new System.Drawing.Size(156, 22);
            this.tB_VoIPEnabled.TabIndex = 38;
            // 
            // tB_HostName
            // 
            this.tB_HostName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tB_HostName.Location = new System.Drawing.Point(92, 22);
            this.tB_HostName.Name = "tB_HostName";
            this.tB_HostName.ReadOnly = true;
            this.tB_HostName.Size = new System.Drawing.Size(156, 22);
            this.tB_HostName.TabIndex = 37;
            // 
            // lb_MaxPlayers
            // 
            this.lb_MaxPlayers.AutoSize = true;
            this.lb_MaxPlayers.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_MaxPlayers.Location = new System.Drawing.Point(134, 137);
            this.lb_MaxPlayers.Name = "lb_MaxPlayers";
            this.lb_MaxPlayers.Size = new System.Drawing.Size(72, 13);
            this.lb_MaxPlayers.TabIndex = 35;
            this.lb_MaxPlayers.Text = "Max Players :";
            // 
            // lb_NumPlayers
            // 
            this.lb_NumPlayers.AutoSize = true;
            this.lb_NumPlayers.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_NumPlayers.Location = new System.Drawing.Point(11, 137);
            this.lb_NumPlayers.Name = "lb_NumPlayers";
            this.lb_NumPlayers.Size = new System.Drawing.Size(75, 13);
            this.lb_NumPlayers.TabIndex = 33;
            this.lb_NumPlayers.Text = "Num Players :";
            // 
            // lb_GameType
            // 
            this.lb_GameType.AutoSize = true;
            this.lb_GameType.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_GameType.Location = new System.Drawing.Point(19, 109);
            this.lb_GameType.Name = "lb_GameType";
            this.lb_GameType.Size = new System.Drawing.Size(67, 13);
            this.lb_GameType.TabIndex = 31;
            this.lb_GameType.Text = "Game Type :";
            // 
            // lb_MapName
            // 
            this.lb_MapName.AutoSize = true;
            this.lb_MapName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_MapName.Location = new System.Drawing.Point(18, 81);
            this.lb_MapName.Name = "lb_MapName";
            this.lb_MapName.Size = new System.Drawing.Size(68, 13);
            this.lb_MapName.TabIndex = 29;
            this.lb_MapName.Text = "Map Name :";
            // 
            // lb_VoIPEnabled
            // 
            this.lb_VoIPEnabled.AutoSize = true;
            this.lb_VoIPEnabled.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_VoIPEnabled.Location = new System.Drawing.Point(6, 53);
            this.lb_VoIPEnabled.Name = "lb_VoIPEnabled";
            this.lb_VoIPEnabled.Size = new System.Drawing.Size(80, 13);
            this.lb_VoIPEnabled.TabIndex = 27;
            this.lb_VoIPEnabled.Text = "VoIP Enabled :";
            // 
            // lb_HostName
            // 
            this.lb_HostName.AutoSize = true;
            this.lb_HostName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_HostName.Location = new System.Drawing.Point(17, 25);
            this.lb_HostName.Name = "lb_HostName";
            this.lb_HostName.Size = new System.Drawing.Size(69, 13);
            this.lb_HostName.TabIndex = 25;
            this.lb_HostName.Text = "Host Name :";
            // 
            // grB_Settings
            // 
            this.grB_Settings.Controls.Add(this.comB_ServerList);
            this.grB_Settings.Controls.Add(this.btn_ResetSettings);
            this.grB_Settings.Controls.Add(this.btn_LoadSettings);
            this.grB_Settings.Controls.Add(this.btn_SaveSettings);
            this.grB_Settings.Controls.Add(this.tB_ServerProtocol);
            this.grB_Settings.Controls.Add(this.tB_ServerPort);
            this.grB_Settings.Controls.Add(this.tB_ServerAddress);
            this.grB_Settings.Controls.Add(this.lb_ServerProtocol);
            this.grB_Settings.Controls.Add(this.lb_ServerPort);
            this.grB_Settings.Controls.Add(this.lb_SelectServer);
            this.grB_Settings.Location = new System.Drawing.Point(8, 194);
            this.grB_Settings.Name = "grB_Settings";
            this.grB_Settings.Size = new System.Drawing.Size(254, 200);
            this.grB_Settings.TabIndex = 14;
            this.grB_Settings.TabStop = false;
            this.grB_Settings.Text = "Settings";
            // 
            // comB_ServerList
            // 
            this.comB_ServerList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comB_ServerList.FormattingEnabled = true;
            this.comB_ServerList.Location = new System.Drawing.Point(102, 16);
            this.comB_ServerList.Name = "comB_ServerList";
            this.comB_ServerList.Size = new System.Drawing.Size(146, 21);
            this.comB_ServerList.TabIndex = 43;
            this.comB_ServerList.Visible = false;
            this.comB_ServerList.SelectedIndexChanged += new System.EventHandler(this.comB_ServerList_SelectedIndexChanged);
            // 
            // btn_ResetSettings
            // 
            this.btn_ResetSettings.Location = new System.Drawing.Point(6, 159);
            this.btn_ResetSettings.Name = "btn_ResetSettings";
            this.btn_ResetSettings.Size = new System.Drawing.Size(242, 23);
            this.btn_ResetSettings.TabIndex = 5;
            this.btn_ResetSettings.Text = "Reset Settings";
            this.btn_ResetSettings.UseVisualStyleBackColor = true;
            this.btn_ResetSettings.Click += new System.EventHandler(this.btn_ResetSettings_Click);
            // 
            // btn_LoadSettings
            // 
            this.btn_LoadSettings.Location = new System.Drawing.Point(6, 130);
            this.btn_LoadSettings.Name = "btn_LoadSettings";
            this.btn_LoadSettings.Size = new System.Drawing.Size(242, 23);
            this.btn_LoadSettings.TabIndex = 4;
            this.btn_LoadSettings.Text = "Load Settings";
            this.btn_LoadSettings.UseVisualStyleBackColor = true;
            this.btn_LoadSettings.Click += new System.EventHandler(this.btn_LoadSettings_Click);
            // 
            // btn_SaveSettings
            // 
            this.btn_SaveSettings.Location = new System.Drawing.Point(6, 101);
            this.btn_SaveSettings.Name = "btn_SaveSettings";
            this.btn_SaveSettings.Size = new System.Drawing.Size(242, 23);
            this.btn_SaveSettings.TabIndex = 3;
            this.btn_SaveSettings.Text = "Save Settings";
            this.btn_SaveSettings.UseVisualStyleBackColor = true;
            this.btn_SaveSettings.Click += new System.EventHandler(this.btn_SaveSettings_Click);
            // 
            // tB_ServerProtocol
            // 
            this.tB_ServerProtocol.Location = new System.Drawing.Point(102, 73);
            this.tB_ServerProtocol.Name = "tB_ServerProtocol";
            this.tB_ServerProtocol.Size = new System.Drawing.Size(146, 22);
            this.tB_ServerProtocol.TabIndex = 2;
            // 
            // tB_ServerPort
            // 
            this.tB_ServerPort.Location = new System.Drawing.Point(102, 45);
            this.tB_ServerPort.Name = "tB_ServerPort";
            this.tB_ServerPort.Size = new System.Drawing.Size(146, 22);
            this.tB_ServerPort.TabIndex = 2;
            // 
            // tB_ServerAddress
            // 
            this.tB_ServerAddress.Location = new System.Drawing.Point(102, 16);
            this.tB_ServerAddress.Name = "tB_ServerAddress";
            this.tB_ServerAddress.Size = new System.Drawing.Size(146, 22);
            this.tB_ServerAddress.TabIndex = 2;
            // 
            // lb_ServerProtocol
            // 
            this.lb_ServerProtocol.AutoSize = true;
            this.lb_ServerProtocol.Location = new System.Drawing.Point(6, 76);
            this.lb_ServerProtocol.Name = "lb_ServerProtocol";
            this.lb_ServerProtocol.Size = new System.Drawing.Size(90, 13);
            this.lb_ServerProtocol.TabIndex = 1;
            this.lb_ServerProtocol.Text = "Server Protocol :";
            // 
            // lb_ServerPort
            // 
            this.lb_ServerPort.AutoSize = true;
            this.lb_ServerPort.Location = new System.Drawing.Point(28, 48);
            this.lb_ServerPort.Name = "lb_ServerPort";
            this.lb_ServerPort.Size = new System.Drawing.Size(68, 13);
            this.lb_ServerPort.TabIndex = 1;
            this.lb_ServerPort.Text = "Server Port :";
            // 
            // lb_SelectServer
            // 
            this.lb_SelectServer.AutoSize = true;
            this.lb_SelectServer.Location = new System.Drawing.Point(8, 19);
            this.lb_SelectServer.Name = "lb_SelectServer";
            this.lb_SelectServer.Size = new System.Drawing.Size(88, 13);
            this.lb_SelectServer.TabIndex = 1;
            this.lb_SelectServer.Text = "Server Address :";
            // 
            // lV_PlayersRed
            // 
            this.lV_PlayersRed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lV_PlayersRed.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colH_RedPlayerUniqueID,
            this.colH_RedPlayerIPAddress,
            this.colH_RedPlayerName,
            this.colH_RedPlayerTeam,
            this.colH_RedPlayerScore,
            this.colH_RedPlayerKills,
            this.colH_RedPlayerDeaths,
            this.colH_RedPlayerAssists});
            this.lV_PlayersRed.ContextMenuStrip = this.cMS_Player;
            this.lV_PlayersRed.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lV_PlayersRed.FullRowSelect = true;
            this.lV_PlayersRed.Location = new System.Drawing.Point(268, 6);
            this.lV_PlayersRed.Name = "lV_PlayersRed";
            this.lV_PlayersRed.Size = new System.Drawing.Size(582, 219);
            this.lV_PlayersRed.TabIndex = 9;
            this.lV_PlayersRed.UseCompatibleStateImageBehavior = false;
            this.lV_PlayersRed.View = System.Windows.Forms.View.Details;
            this.lV_PlayersRed.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lV_Players_ColumnClick);
            // 
            // colH_RedPlayerUniqueID
            // 
            this.colH_RedPlayerUniqueID.Tag = "120";
            this.colH_RedPlayerUniqueID.Text = "Unique ID";
            this.colH_RedPlayerUniqueID.Width = 120;
            // 
            // colH_RedPlayerIPAddress
            // 
            this.colH_RedPlayerIPAddress.Tag = "97";
            this.colH_RedPlayerIPAddress.Text = "IP Address";
            this.colH_RedPlayerIPAddress.Width = 97;
            // 
            // colH_RedPlayerName
            // 
            this.colH_RedPlayerName.Tag = "115";
            this.colH_RedPlayerName.Text = "Name";
            this.colH_RedPlayerName.Width = 115;
            // 
            // colH_RedPlayerTeam
            // 
            this.colH_RedPlayerTeam.Tag = "45";
            this.colH_RedPlayerTeam.Text = "Team";
            this.colH_RedPlayerTeam.Width = 54;
            // 
            // colH_RedPlayerScore
            // 
            this.colH_RedPlayerScore.Tag = "45";
            this.colH_RedPlayerScore.Text = "Score";
            this.colH_RedPlayerScore.Width = 45;
            // 
            // colH_RedPlayerKills
            // 
            this.colH_RedPlayerKills.Tag = "45";
            this.colH_RedPlayerKills.Text = "Kills";
            this.colH_RedPlayerKills.Width = 45;
            // 
            // colH_RedPlayerDeaths
            // 
            this.colH_RedPlayerDeaths.Tag = "50";
            this.colH_RedPlayerDeaths.Text = "Deaths";
            this.colH_RedPlayerDeaths.Width = 50;
            // 
            // colH_RedPlayerAssists
            // 
            this.colH_RedPlayerAssists.Tag = "50";
            this.colH_RedPlayerAssists.Text = "Assists";
            this.colH_RedPlayerAssists.Width = 50;
            // 
            // btn_QuickSettings
            // 
            this.btn_QuickSettings.Location = new System.Drawing.Point(162, 401);
            this.btn_QuickSettings.Name = "btn_QuickSettings";
            this.btn_QuickSettings.Size = new System.Drawing.Size(100, 23);
            this.btn_QuickSettings.TabIndex = 13;
            this.btn_QuickSettings.Text = "Quick Settings";
            this.btn_QuickSettings.UseVisualStyleBackColor = true;
            this.btn_QuickSettings.Click += new System.EventHandler(this.btn_QuickSettings_Click);
            // 
            // btn_Disconnect
            // 
            this.btn_Disconnect.Location = new System.Drawing.Point(81, 400);
            this.btn_Disconnect.Name = "btn_Disconnect";
            this.btn_Disconnect.Size = new System.Drawing.Size(75, 23);
            this.btn_Disconnect.TabIndex = 12;
            this.btn_Disconnect.Text = "Disconnect";
            this.btn_Disconnect.UseVisualStyleBackColor = true;
            this.btn_Disconnect.Click += new System.EventHandler(this.btn_Disconnect_Click);
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.Location = new System.Drawing.Point(147, 428);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(115, 23);
            this.btn_Refresh.TabIndex = 10;
            this.btn_Refresh.Text = "Manual Refresh";
            this.btn_Refresh.UseVisualStyleBackColor = true;
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // btn_Connect
            // 
            this.btn_Connect.Location = new System.Drawing.Point(8, 400);
            this.btn_Connect.Name = "btn_Connect";
            this.btn_Connect.Size = new System.Drawing.Size(67, 23);
            this.btn_Connect.TabIndex = 5;
            this.btn_Connect.Text = "Connect";
            this.btn_Connect.UseVisualStyleBackColor = true;
            this.btn_Connect.Click += new System.EventHandler(this.btn_Connect_Click);
            // 
            // tP_Console
            // 
            this.tP_Console.Controls.Add(this.btn_ChangeTime);
            this.tP_Console.Controls.Add(this.rTB_HiddenOutput);
            this.tP_Console.Controls.Add(this.btn_ConsoleSend);
            this.tP_Console.Controls.Add(this.tB_ConsoleInput);
            this.tP_Console.Controls.Add(this.rTB_ConsoleOutput);
            this.tP_Console.Location = new System.Drawing.Point(4, 22);
            this.tP_Console.Name = "tP_Console";
            this.tP_Console.Padding = new System.Windows.Forms.Padding(3);
            this.tP_Console.Size = new System.Drawing.Size(858, 476);
            this.tP_Console.TabIndex = 1;
            this.tP_Console.Text = "Console";
            this.tP_Console.UseVisualStyleBackColor = true;
            // 
            // btn_ChangeTime
            // 
            this.btn_ChangeTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_ChangeTime.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ChangeTime.Location = new System.Drawing.Point(8, 431);
            this.btn_ChangeTime.Name = "btn_ChangeTime";
            this.btn_ChangeTime.Size = new System.Drawing.Size(88, 24);
            this.btn_ChangeTime.TabIndex = 4;
            this.btn_ChangeTime.Text = "Local Time";
            this.btn_ChangeTime.UseVisualStyleBackColor = true;
            this.btn_ChangeTime.Click += new System.EventHandler(this.btn_ChangeTime_Click);
            // 
            // rTB_HiddenOutput
            // 
            this.rTB_HiddenOutput.Location = new System.Drawing.Point(766, 6);
            this.rTB_HiddenOutput.Name = "rTB_HiddenOutput";
            this.rTB_HiddenOutput.Size = new System.Drawing.Size(86, 20);
            this.rTB_HiddenOutput.TabIndex = 3;
            this.rTB_HiddenOutput.Text = "Hidden Output";
            this.rTB_HiddenOutput.Visible = false;
            // 
            // btn_ConsoleSend
            // 
            this.btn_ConsoleSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_ConsoleSend.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ConsoleSend.Location = new System.Drawing.Point(772, 431);
            this.btn_ConsoleSend.Name = "btn_ConsoleSend";
            this.btn_ConsoleSend.Size = new System.Drawing.Size(78, 24);
            this.btn_ConsoleSend.TabIndex = 2;
            this.btn_ConsoleSend.Text = "Send";
            this.btn_ConsoleSend.UseVisualStyleBackColor = true;
            this.btn_ConsoleSend.Click += new System.EventHandler(this.btn_ConsoleSend_Click);
            // 
            // tB_ConsoleInput
            // 
            this.tB_ConsoleInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tB_ConsoleInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tB_ConsoleInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.tB_ConsoleInput.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tB_ConsoleInput.Location = new System.Drawing.Point(102, 432);
            this.tB_ConsoleInput.Name = "tB_ConsoleInput";
            this.tB_ConsoleInput.Size = new System.Drawing.Size(664, 22);
            this.tB_ConsoleInput.TabIndex = 1;
            this.tB_ConsoleInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tB_ConsoleInput_KeyDown);
            // 
            // rTB_ConsoleOutput
            // 
            this.rTB_ConsoleOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rTB_ConsoleOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rTB_ConsoleOutput.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rTB_ConsoleOutput.HideSelection = false;
            this.rTB_ConsoleOutput.Location = new System.Drawing.Point(8, 6);
            this.rTB_ConsoleOutput.Name = "rTB_ConsoleOutput";
            this.rTB_ConsoleOutput.ReadOnly = true;
            this.rTB_ConsoleOutput.Size = new System.Drawing.Size(842, 420);
            this.rTB_ConsoleOutput.TabIndex = 0;
            this.rTB_ConsoleOutput.Text = "";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(113, 6);
            // 
            // stS_First
            // 
            this.stS_First.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSSL_First});
            this.stS_First.Location = new System.Drawing.Point(0, 480);
            this.stS_First.Name = "stS_First";
            this.stS_First.Size = new System.Drawing.Size(866, 22);
            this.stS_First.TabIndex = 15;
            this.stS_First.Text = "statusStrip1";
            // 
            // tSSL_First
            // 
            this.tSSL_First.Name = "tSSL_First";
            this.tSSL_First.Size = new System.Drawing.Size(0, 17);
            // 
            // bgW_GetPlayerList
            // 
            this.bgW_GetPlayerList.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgW_GetPlayerList_DoWork);
            // 
            // bgW_GetServerInfo
            // 
            this.bgW_GetServerInfo.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgW_GetServerInfo_DoWork);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 502);
            this.Controls.Add(this.stS_First);
            this.Controls.Add(this.tCtrl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(882, 541);
            this.Name = "MainForm";
            this.Text = "Dewricon";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tCtrl.ResumeLayout(false);
            this.tP_Players.ResumeLayout(false);
            this.tP_Players.PerformLayout();
            this.cMS_Player.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nUD_RefreshInterval)).EndInit();
            this.grB_ServerInfo.ResumeLayout(false);
            this.grB_ServerInfo.PerformLayout();
            this.grB_Settings.ResumeLayout(false);
            this.grB_Settings.PerformLayout();
            this.tP_Console.ResumeLayout(false);
            this.tP_Console.PerformLayout();
            this.stS_First.ResumeLayout(false);
            this.stS_First.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

#endregion

        private System.Windows.Forms.TabControl tCtrl;
        private System.Windows.Forms.TabPage tP_Players;
        private System.Windows.Forms.Button btn_Connect;
        private System.Windows.Forms.TabPage tP_Console;
        internal System.Windows.Forms.RichTextBox rTB_ConsoleOutput;
        private System.Windows.Forms.Button btn_ConsoleSend;
        private System.Windows.Forms.TextBox tB_ConsoleInput;
        private System.Windows.Forms.ColumnHeader colH_RedPlayerName;
        private System.Windows.Forms.ColumnHeader colH_RedPlayerScore;
        private System.Windows.Forms.ColumnHeader colH_RedPlayerKills;
        private System.Windows.Forms.ColumnHeader colH_RedPlayerAssists;
        private System.Windows.Forms.Button btn_Refresh;
        private System.Windows.Forms.ColumnHeader colH_RedPlayerDeaths;
        private System.Windows.Forms.ColumnHeader colH_RedPlayerUniqueID;
        private System.Windows.Forms.GroupBox grB_ServerInfo;
        private System.Windows.Forms.Label lb_MaxPlayers;
        private System.Windows.Forms.Label lb_NumPlayers;
        private System.Windows.Forms.Label lb_GameType;
        private System.Windows.Forms.Label lb_MapName;
        private System.Windows.Forms.Label lb_VoIPEnabled;
        private System.Windows.Forms.Label lb_HostName;
        private System.Windows.Forms.Button btn_Disconnect;
        public System.Windows.Forms.ListView lV_PlayersRed;
        private System.Windows.Forms.Button btn_QuickSettings;
        private System.Windows.Forms.StatusStrip stS_First;
        private System.Windows.Forms.ToolStripStatusLabel tSSL_First;
        private System.Windows.Forms.ContextMenuStrip cMS_Player;
        private System.Windows.Forms.ToolStripMenuItem tMSMI_KickPlayer;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.TextBox tB_HostName;
        private System.Windows.Forms.TextBox tB_MaxPlayers;
        private System.Windows.Forms.TextBox tB_NumPlayers;
        private System.Windows.Forms.TextBox tB_GameType;
        private System.Windows.Forms.TextBox tB_MapName;
        private System.Windows.Forms.TextBox tB_VoIPEnabled;
        private System.Windows.Forms.GroupBox grB_Settings;
        private System.Windows.Forms.Button btn_SaveSettings;
        private System.Windows.Forms.TextBox tB_ServerProtocol;
        private System.Windows.Forms.TextBox tB_ServerPort;
        private System.Windows.Forms.TextBox tB_ServerAddress;
        private System.Windows.Forms.Label lb_ServerProtocol;
        private System.Windows.Forms.Label lb_ServerPort;
        private System.Windows.Forms.Label lb_SelectServer;
        private System.Windows.Forms.Button btn_LoadSettings;
        private System.Windows.Forms.Button btn_ResetSettings;
        private System.Windows.Forms.ColumnHeader colH_RedPlayerIPAddress;
        private System.ComponentModel.BackgroundWorker bgW_GetPlayerList;
        internal System.Windows.Forms.RichTextBox rTB_HiddenOutput;
        private System.ComponentModel.BackgroundWorker bgW_GetServerInfo;
        private System.Windows.Forms.NumericUpDown nUD_RefreshInterval;
        private System.Windows.Forms.Label lb_Refresh;
        private System.Windows.Forms.ToolStripMenuItem tMSMI_BanPlayer;
        private System.Windows.Forms.Button btn_ChangeTime;
        private System.Windows.Forms.ColumnHeader colH_RedPlayerTeam;
        private System.Windows.Forms.ComboBox comB_ServerList;
        public System.Windows.Forms.ListView lV_PlayersBlue;
        private System.Windows.Forms.ColumnHeader colH_BluePlayerUniqueID;
        private System.Windows.Forms.ColumnHeader colH_BluePlayerIPAddress;
        private System.Windows.Forms.ColumnHeader colH_BluePlayerName;
        private System.Windows.Forms.ColumnHeader colH_BluePlayerTeam;
        private System.Windows.Forms.ColumnHeader colH_BluePlayerScore;
        private System.Windows.Forms.ColumnHeader colH_BluePLayerKills;
        private System.Windows.Forms.ColumnHeader colH_BluePlayerDeaths;
        private System.Windows.Forms.ColumnHeader colH_BluePlayerAssists;
    }
}


﻿using System;
using System.Windows.Forms;

namespace Dewricon
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Forms.f_Main = new MainForm();
            Forms.f_Settings = new SettingsForm();
            Application.Run(Forms.f_Main);
        }
    }

    public class Forms
    {
        public static MainForm f_Main;
        public static SettingsForm f_Settings;
    }
}

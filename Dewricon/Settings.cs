﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Dewricon
{
    public partial class SettingsForm : Form
    {
        MainForm f_Main = Forms.f_Main;

        public static string ServerName;
        public static int MaxPlayers;
        public static int Sprint;
        public static int UnlimitedSprint;
        public static int Assassination;
        public static int VoIP;

        public static List<object> SettingsList = new List<object>();

        public SettingsForm()
        {
            InitializeComponent();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            loadSettings();
        }

        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
        }

        private void tB_ServerName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SaveSetting("Server.Name ", tB_ServerName.Text);
        }

        private void chB_Sprint_CheckedChanged(object sender, EventArgs e)
        {
            SaveSetting("Server.SprintEnabled ", Convert.ToInt32(chB_Sprint.Checked));
        }

        private void chB_UnlimitedSprint_CheckedChanged(object sender, EventArgs e)
        {
            SaveSetting("Server.UnlimitedSprint ", Convert.ToInt32(chB_UnlimitedSprint.Checked));
        }

        private void nUD_MaxPlayers_ValueChanged(object sender, EventArgs e)
        {
            SaveSetting("Server.MaxPlayers ", nUD_MaxPlayers.Value);
        }

        private void chB_Assassination_CheckedChanged(object sender, EventArgs e)
        {
            SaveSetting("Server.AssassinationEnabled ", Convert.ToInt32(chB_Assassination.Checked));
        }

        private void chB_VoIP_CheckedChanged(object sender, EventArgs e)
        {
            SaveSetting("VoIP.ServerEnabled ", Convert.ToInt32(chB_VoIP.Checked));
        }

        private void btn_SaveSettings_Click(object sender, EventArgs e)
        {
            new WebSockets().Send("WriteConfig");
        }

        public class Settings
        {
            public string Name { get; set; }
            public string Sprint { get; set; }
            public string UnlimitedSprint { get; set; }
            public string Assassination { get; set; }
            public string MaxPlayers { get; set; }
            public string VoIP { get; set; }
            public string[] ToSettingsList()
            {
                return new string[] {
                    Name.ToString(),
                    Sprint.ToString(),
                    UnlimitedSprint.ToString(),
                    Assassination.ToString(),
                    MaxPlayers.ToString(),
                    VoIP.ToString()
                };
            }
        }

        public void GetSettings(string name, int voip, int sprint, int unlimitedsprint, int assassination, int maxplayers)
        {
            List<Settings> settingItems = new List<Settings>();
            SettingsList.AddRange(settingItems.ToList());

            ServerName = name;
            Sprint = sprint;
            UnlimitedSprint = unlimitedsprint;
            Assassination = assassination;
            MaxPlayers = maxplayers;
            VoIP = voip;
        }

        public void loadSettings() {
            tB_ServerName.Text = ServerName;
            
            new WebSockets().ConsoleOutput = f_Main.rTB_HiddenOutput;

            if (new Helper().GetRconMessage("VoIP.ServerEnabled") == 1)
                chB_VoIP.Checked = true;
            else
                chB_VoIP.Checked = false;

            if (new Helper().GetRconMessage("Server.SprintEnabled") == 1)
                chB_Sprint.Checked = true;
            else
                chB_Sprint.Checked = false;

            if (new Helper().GetRconMessage("Server.UnlimitedSprint") == 1)
                chB_UnlimitedSprint.Checked = true;
            else
                chB_UnlimitedSprint.Checked = false;

            if (new Helper().GetRconMessage("Server.AssassinationEnabled") == 1)
                chB_Assassination.Checked = true;
            else
                chB_Assassination.Checked = false;

            if (nUD_MaxPlayers.Value != 0)
                nUD_MaxPlayers.Value = new Helper().GetRconMessage("Server.MaxPlayers");

            new WebSockets().ConsoleOutput = f_Main.rTB_ConsoleOutput;
        }

        public void SaveSetting(string Setting, object Value)
        {
            new WebSockets().Send(Setting + " \"" + Value.ToString() + "\"");
        }
    }
}

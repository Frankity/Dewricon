﻿namespace Dewricon
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chB_VoIP = new System.Windows.Forms.CheckBox();
            this.btn_SaveSettings = new System.Windows.Forms.Button();
            this.lb_ServerName = new System.Windows.Forms.Label();
            this.tB_ServerName = new System.Windows.Forms.TextBox();
            this.chB_Sprint = new System.Windows.Forms.CheckBox();
            this.nUD_MaxPlayers = new System.Windows.Forms.NumericUpDown();
            this.lb_MaxPlayers = new System.Windows.Forms.Label();
            this.chB_UnlimitedSprint = new System.Windows.Forms.CheckBox();
            this.chB_Assassination = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_MaxPlayers)).BeginInit();
            this.SuspendLayout();
            // 
            // chB_VoIP
            // 
            this.chB_VoIP.AutoSize = true;
            this.chB_VoIP.Location = new System.Drawing.Point(114, 65);
            this.chB_VoIP.Name = "chB_VoIP";
            this.chB_VoIP.Size = new System.Drawing.Size(48, 17);
            this.chB_VoIP.TabIndex = 10;
            this.chB_VoIP.Text = "VoIP";
            this.chB_VoIP.UseVisualStyleBackColor = true;
            this.chB_VoIP.CheckedChanged += new System.EventHandler(this.chB_VoIP_CheckedChanged);
            // 
            // btn_SaveSettings
            // 
            this.btn_SaveSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_SaveSettings.Location = new System.Drawing.Point(258, 62);
            this.btn_SaveSettings.Name = "btn_SaveSettings";
            this.btn_SaveSettings.Size = new System.Drawing.Size(58, 24);
            this.btn_SaveSettings.TabIndex = 9;
            this.btn_SaveSettings.Text = "Save";
            this.btn_SaveSettings.UseVisualStyleBackColor = true;
            this.btn_SaveSettings.Click += new System.EventHandler(this.btn_SaveSettings_Click);
            // 
            // lb_ServerName
            // 
            this.lb_ServerName.AutoSize = true;
            this.lb_ServerName.Location = new System.Drawing.Point(9, 15);
            this.lb_ServerName.Name = "lb_ServerName";
            this.lb_ServerName.Size = new System.Drawing.Size(70, 13);
            this.lb_ServerName.TabIndex = 8;
            this.lb_ServerName.Text = "Server Name";
            // 
            // tB_ServerName
            // 
            this.tB_ServerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tB_ServerName.Location = new System.Drawing.Point(88, 12);
            this.tB_ServerName.Name = "tB_ServerName";
            this.tB_ServerName.Size = new System.Drawing.Size(228, 22);
            this.tB_ServerName.TabIndex = 7;
            this.tB_ServerName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tB_ServerName_KeyDown);
            // 
            // chB_Sprint
            // 
            this.chB_Sprint.AutoSize = true;
            this.chB_Sprint.Location = new System.Drawing.Point(12, 42);
            this.chB_Sprint.Name = "chB_Sprint";
            this.chB_Sprint.Size = new System.Drawing.Size(57, 17);
            this.chB_Sprint.TabIndex = 10;
            this.chB_Sprint.Text = "Sprint";
            this.chB_Sprint.UseVisualStyleBackColor = true;
            this.chB_Sprint.CheckedChanged += new System.EventHandler(this.chB_Sprint_CheckedChanged);
            // 
            // nUD_MaxPlayers
            // 
            this.nUD_MaxPlayers.Location = new System.Drawing.Point(191, 40);
            this.nUD_MaxPlayers.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.nUD_MaxPlayers.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUD_MaxPlayers.Name = "nUD_MaxPlayers";
            this.nUD_MaxPlayers.Size = new System.Drawing.Size(49, 22);
            this.nUD_MaxPlayers.TabIndex = 11;
            this.nUD_MaxPlayers.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nUD_MaxPlayers.ValueChanged += new System.EventHandler(this.nUD_MaxPlayers_ValueChanged);
            // 
            // lb_MaxPlayers
            // 
            this.lb_MaxPlayers.AutoSize = true;
            this.lb_MaxPlayers.Location = new System.Drawing.Point(246, 43);
            this.lb_MaxPlayers.Name = "lb_MaxPlayers";
            this.lb_MaxPlayers.Size = new System.Drawing.Size(63, 13);
            this.lb_MaxPlayers.TabIndex = 12;
            this.lb_MaxPlayers.Text = "MaxPlayers";
            // 
            // chB_UnlimitedSprint
            // 
            this.chB_UnlimitedSprint.AutoSize = true;
            this.chB_UnlimitedSprint.Location = new System.Drawing.Point(75, 42);
            this.chB_UnlimitedSprint.Name = "chB_UnlimitedSprint";
            this.chB_UnlimitedSprint.Size = new System.Drawing.Size(110, 17);
            this.chB_UnlimitedSprint.TabIndex = 13;
            this.chB_UnlimitedSprint.Text = "Unlimited Sprint";
            this.chB_UnlimitedSprint.UseVisualStyleBackColor = true;
            this.chB_UnlimitedSprint.CheckedChanged += new System.EventHandler(this.chB_UnlimitedSprint_CheckedChanged);
            // 
            // chB_Assassination
            // 
            this.chB_Assassination.AutoSize = true;
            this.chB_Assassination.Location = new System.Drawing.Point(12, 65);
            this.chB_Assassination.Name = "chB_Assassination";
            this.chB_Assassination.Size = new System.Drawing.Size(96, 17);
            this.chB_Assassination.TabIndex = 14;
            this.chB_Assassination.Text = "Assassination";
            this.chB_Assassination.UseVisualStyleBackColor = true;
            this.chB_Assassination.CheckedChanged += new System.EventHandler(this.chB_Assassination_CheckedChanged);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 98);
            this.Controls.Add(this.chB_Assassination);
            this.Controls.Add(this.chB_UnlimitedSprint);
            this.Controls.Add(this.lb_MaxPlayers);
            this.Controls.Add(this.nUD_MaxPlayers);
            this.Controls.Add(this.chB_Sprint);
            this.Controls.Add(this.chB_VoIP);
            this.Controls.Add(this.btn_SaveSettings);
            this.Controls.Add(this.lb_ServerName);
            this.Controls.Add(this.tB_ServerName);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MinimumSize = new System.Drawing.Size(262, 39);
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nUD_MaxPlayers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_SaveSettings;
        private System.Windows.Forms.Label lb_ServerName;
        private System.Windows.Forms.Label lb_MaxPlayers;
        public System.Windows.Forms.TextBox tB_ServerName;
        private System.Windows.Forms.CheckBox chB_VoIP;
        private System.Windows.Forms.CheckBox chB_Sprint;
        private System.Windows.Forms.NumericUpDown nUD_MaxPlayers;
        private System.Windows.Forms.CheckBox chB_UnlimitedSprint;
        private System.Windows.Forms.CheckBox chB_Assassination;
    }
}
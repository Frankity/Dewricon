﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using SuperSocket.ClientEngine;
using WebSocket4Net;
using System.Collections;
using Newtonsoft.Json;

namespace Dewricon
{
    public class WebSockets
    {
        #region Variables
        public WebSocket socket;
        public bool isConnected = false;

        public string lastCommand = "";
        public string lastMessage = "";

        public List<string> Commands = new List<string>();
        public TextBox ConsoleInput;
        public RichTextBox ConsoleOutput;
        public RichTextBox HiddenOutput;
        public RichTextBox Log;

        public string Address {
            get { return Properties.Settings.Default.ServerAddress; }
            set { Properties.Settings.Default.ServerAddress = value; }
        }
        public int Port {
            get { return Properties.Settings.Default.ServerPort; }
            set { Properties.Settings.Default.ServerPort = value; }
        }
        public string Protocol {
            get { return Properties.Settings.Default.ServerProtocol; }
            set { Properties.Settings.Default.ServerProtocol = value; }
        }
        #endregion

        #region Socket Operations
        internal void Socket_Opened(object sender, EventArgs e)
        {
            isConnected = true;
            string logFormatted = String.Format("[{1}]: Socket Opened{0}", Environment.NewLine, new Helper().DateTimeNow);
            string conFormatted = String.Format("[CON]: Connected Successfully!{0}", Environment.NewLine);

            Log.Invoke(new Action(() => Log.AppendText(logFormatted)));
            //ConsoleOutput.Invoke(new Action(() => ConsoleOutput.AppendText(conFormatted)));

            if (Commands.Any() == false)
                socket.Send("Help");
        }

        internal void Socket_Closed(object sender, EventArgs e)
        {
            isConnected = false;
            string logFormatted = String.Format("[{1}]: Socket Closed{0}", Environment.NewLine, new Helper().DateTimeNow);
            string conFormatted = String.Format("[CON]: Disconnected Successfully!{0}", Environment.NewLine);

            Log.Invoke(new Action(() => Log.AppendText(logFormatted)));
            //ConsoleOutput.Invoke(new Action(() => ConsoleOutput.AppendText(conFormatted)));
        }

        internal void Socket_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            if (Commands.Any() == false)
            {
                for (int i = 0; i < e.Message.Split('\n').Length; i++)
                    Commands.Add(e.Message.Split('\n')[i].Split(' ')[0]);

                ConsoleInput.Invoke(new Action(() => ConsoleInput.AutoCompleteCustomSource.AddRange(Commands.ToArray())));
            }
            else
            {
                lastMessage = e.Message;
                string logFormatted = String.Format("[{1}]: Message Received{0}", Environment.NewLine, new Helper().DateTimeNow);
                string recFormatted = String.Format("[REC]:{1}{0}{1}", e.Message, Environment.NewLine);
                ConsoleOutput.Invoke(new Action(() => {
                    Log.AppendText(recFormatted);
                    //ConsoleOutput.AppendText(recFormatted);
                    ConsoleOutput.ScrollToCaret();
                }));
            }
            lastMessage = e.Message;
        }

        internal void Socket_Error(object sender, ErrorEventArgs e)
        {
            isConnected = false;
            ConsoleOutput.Invoke(new Action(() => {
                ConsoleOutput.AppendText(e.Exception + Environment.NewLine);
                ConsoleOutput.ScrollToCaret();
            }));
            StartConnection();
        }
        #endregion

        #region Functions
        public void StartConnection()
        {
            Log = ConsoleOutput;
            try
            {
                socket = new WebSocket("ws://" + Address + ":" + Port, Protocol);

                socket.Opened += new EventHandler(Socket_Opened);
                socket.Closed += new EventHandler(Socket_Closed);
                socket.MessageReceived += Socket_MessageReceived;
                socket.Error += Socket_Error;
            }
            catch (Exception)
            {
                ConsoleOutput.Invoke(new Action(() => ConsoleOutput.AppendText("Are you sure you're connected?")));
            }
        }

        public void Send(string Input)
        {
            if (isConnected)
            {
                string logFormatted = String.Format("[{2}]: {1}{0}", Environment.NewLine, Input, new Helper().DateTimeNow);
                string sentFormatted = String.Format("[SENT]: {1}{0}", Environment.NewLine, Input);

                switch (Input.ToLower())
                {
                    case "/listcommands":
                        Log.Invoke(new Action(() => Log.AppendText(logFormatted)));
                        Commands.ForEach(Command => ConsoleOutput.Invoke(new Action(() => ConsoleOutput.AppendText(logFormatted))));
                        break;
                    case "/clear":
                        Log.Invoke(new Action(() => Log.AppendText(logFormatted)));
                        ConsoleOutput.Invoke(new Action(() => ConsoleOutput.Clear()));
                        break;
                    case "/connect":
                        Log.Invoke(new Action(() => Log.AppendText(logFormatted)));
                        Connection(true);
                        break;
                    case "/disconnect":
                        Log.Invoke(new Action(() => Log.AppendText(logFormatted)));
                        Connection(false);
                        break;
                    default:
                        Log.Invoke(new Action(() => Log.AppendText(logFormatted)));
                        //ConsoleOutput.Invoke(new Action(() => ConsoleOutput.AppendText(logFormatted)));
                        socket.Send(Input);
                        break;
                }

                lastCommand = Input;
                ConsoleInput.Invoke(new Action(() => ConsoleInput.Clear()));
            }
        }

        public void Connection(bool action)
        {
            switch (action)
            {
                case true:
                    try { socket.Open(); } catch (Exception ex) { MessageBox.Show(ex.Message); }
                    break;
                case false:
                    try { socket.Close(); } catch (Exception) { }
                    break;
            }
        }
        #endregion
    }

    public class Helper
    {
        public bool isNull(object toCheck)
        {
            if (toCheck == null)
                return true;

            return false;
        }

        public dynamic GetRconMessage(string Input)
        {
            string response = null;

            foreach (string Command in new WebSockets().Commands)
            {
                if (Input == Command)
                {
                    new WebSockets().Send(Input);
                    Thread.Sleep(500);

                    if (new WebSockets().lastCommand == Input && String.IsNullOrEmpty(new WebSockets().lastMessage) == false)
                        response = new WebSockets().lastMessage;
                }
            }

            return response;
        }

        public dynamic GetDewJson(string Url)
        {
            if (Url.ToLower().Contains("http"))
                return JsonConvert.DeserializeObject(new WebClient().DownloadString(new Uri("http://" + Url.Split(':')[1] + ":11775")));
            else
                return JsonConvert.DeserializeObject(new WebClient().DownloadString(new Uri("http://" + Url + ":11775")));
        }

        public dynamic GetServersJson(string Url)
        {
            dynamic Master = JsonConvert.DeserializeObject(new WebClient().DownloadString(new Uri("http://" + Url + "/list")));

            List<string> Servers = new List<string>();
            foreach (string Server in Master.result.servers)
            {
                Servers.Add(Server.Split(':')[0]);
            }

            return Servers;
        }

        public bool DateTimeUtc
        {
            get { return Properties.Settings.Default.DateTimeUtc; }
            set { Properties.Settings.Default.DateTimeUtc = value; }
        }

        public int RefreshInterval
        {
            get { return Properties.Settings.Default.RefreshInterval; }
            set { Properties.Settings.Default.RefreshInterval = value; }
        }

        public DateTime DateTimeNow;

        public class ListViewItemComparer : IComparer
        {
            private int col;
            private int ipdex;
            private SortOrder order;
            public ListViewItemComparer()
            {
                col = 0;
                order = SortOrder.Ascending;
            }
            public ListViewItemComparer(int column, SortOrder order, int ipindex)
            {
                col = column;
                this.order = order;
                ipdex = ipindex;
            }
            public int Compare(object x, object y)
            {
                int returnVal = -1;
                if (new WebSockets().isConnected)
                    returnVal = String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
                else
                    if (col != ipdex)
                    returnVal = String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
                if (order == SortOrder.Descending)
                    returnVal *= -1;
                return returnVal;
            }
        }
    }


    /// <summary>
    /// Parse strings into true or false bools using relaxed parsing rules
    /// </summary>
    public static class BoolParser
    {
        /// <summary>
        /// Get the boolean value for this string
        /// </summary>
        public static bool GetValue(string value)
        {
            return IsTrue(value);
        }

        /// <summary>
        /// Determine whether the string is not True
        /// </summary>
        public static bool IsFalse(string value)
        {
            return !IsTrue(value);
        }

        /// <summary>
        /// Determine whether the string is equal to True
        /// </summary>
        public static bool IsTrue(string value)
        {
            try
            {
                if (value == null)
                    return false;
                
                value = value.Trim();
                
                value = value.ToLower();

                switch (value.Trim().ToLower())
                {
                    case "true":
                    case "t":
                    case "1":
                    case "yes":
                    case "y":
                        return true;
                    default:
                        return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }


    public partial class HttpServer
    {
        private HttpListener _listener;
        private Thread _thread;

        public HttpServer()
        {
            _listener = new HttpListener();
            _listener.Prefixes.Add("http://+:1434/");
        }

        public void Start()
        {
            _listener.Start();
            _thread = new Thread(new ThreadStart(Run));
            _thread.Start();
        }

        public void Run()
        {
            while (true)
            {
                Thread.Sleep(1);

                try
                {
                    var context = _listener.GetContext();
                    ThreadPool.QueueUserWorkItem(state =>
                        {
                            OnResponse((HttpListenerContext)state);
                        }, context);
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }
            }
        }

        public void OnResponse(HttpListenerContext context)
        {
            var url = context.Request.Url.PathAndQuery;
            Console.WriteLine(string.Format("HTTP request form {0}", url));
            var urlparts = url.Substring(1).Split(new[] { '/' }, 2);
            if (urlparts.Length >= 1)
            {
                if (urlparts[0] == "players")
                {
                    new WebSockets().ConsoleOutput.Invoke(new Action(() => new WebSockets().ConsoleOutput.AppendText("lol")));

                    List<string> li = new List<string>();
                    foreach (var item in MainForm.Player_List)
                        li.Add(item);

                    for (int i = 0; i < MainForm.Player_List.Count; i++)
                    {
                        var derp = li[i];
                        new WebSockets().ConsoleOutput.Invoke(new Action(() => new WebSockets().ConsoleOutput.AppendText(derp)));
                        var b = Encoding.ASCII.GetBytes(derp.ToString());
                        context.Response.ContentLength64 = b.Length;
                        context.Response.ContentType = "text/plain";
                        context.Response.OutputStream.Write(b, 0, b.Length);
                        context.Response.OutputStream.Close();
                        return;
                    }
                }
            }
        }
    }
}

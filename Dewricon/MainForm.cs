﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Dewricon
{
    public partial class MainForm : Form
    {
        public static WebSockets dewRcon = new WebSockets();

        public MainForm()
        {
            InitializeComponent();

            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
            tSSL_First.Text = "Version: " + fileVersionInfo.FileVersion;
        }

        #region
        private void MainForm_Load(object sender, EventArgs e)
        {
#if DEBUG
            tB_ServerAddress.Visible = false;
            lb_SelectServer.Location = new System.Drawing.Point(19, 19);
            lb_SelectServer.Text = "Select Server :";
            comB_ServerList.Visible = true;
#endif
            GetServerList();
            if (new Helper().DateTimeUtc)
            {
                new Helper().DateTimeNow = DateTime.UtcNow;
                btn_ChangeTime.Text = "Local Time";
                btn_ChangeTime.Tag = "Universal Time";
            }
            else
            {
                new Helper().DateTimeNow = DateTime.Now;
                btn_ChangeTime.Text = "Universal Time";
                btn_ChangeTime.Tag = "Local Time";
            }

            nUD_RefreshInterval.Value = new Helper().RefreshInterval;

            dewRcon.ConsoleOutput = rTB_ConsoleOutput;
            dewRcon.HiddenOutput = rTB_HiddenOutput;
            dewRcon.ConsoleInput = tB_ConsoleInput;

            dewRcon.StartConnection();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
        }
        #endregion
        #region Players Tab

        private void btn_Connect_Click(object sender, EventArgs e)
        {
            dewRcon.Connection(true);

            comB_ServerList.Text = tB_ServerAddress.Text;

            if (nUD_RefreshInterval.Value != 0 && !bgW_GetServerInfo.IsBusy)
                bgW_GetServerInfo.RunWorkerAsync(nUD_RefreshInterval.Value);
        }

        private void btn_Disconnect_Click(object sender, EventArgs e)
        {
            dewRcon.Connection(false);
        }

        private void btn_QuickSettings_Click(object sender, EventArgs e)
        {
            if (dewRcon.isConnected)
            {
                if (!bgW_GetServerInfo.IsBusy)
                    bgW_GetServerInfo.RunWorkerAsync();

                tCtrl.SelectedTab = tP_Console;
                new SettingsForm().ShowDialog();
            }
        }

        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            if (dewRcon.isConnected)
                if (bgW_GetServerInfo.IsBusy == false)
                    bgW_GetServerInfo.RunWorkerAsync(nUD_RefreshInterval.Value);
        }

        private void nUD_RefreshInterval_ValueChanged(object sender, EventArgs e)
        {
            new Helper().RefreshInterval = (int)nUD_RefreshInterval.Value;

            if (dewRcon.isConnected)
                if (bgW_GetServerInfo.IsBusy && nUD_RefreshInterval.Value == 0)
                    bgW_GetServerInfo.Dispose();
                else if (!bgW_GetServerInfo.IsBusy)
                    bgW_GetServerInfo.RunWorkerAsync(nUD_RefreshInterval.Value);
        }

        private void btn_SaveSettings_Click(object sender, EventArgs e)
        {
            dewRcon.Address = tB_ServerAddress.Text;
            dewRcon.Port = Int32.Parse(tB_ServerPort.Text);
            dewRcon.Protocol = tB_ServerProtocol.Text;

            Properties.Settings.Default.Save();
        }

        private void btn_LoadSettings_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Reload();

            tB_ServerAddress.Text = dewRcon.Address;
            tB_ServerPort.Text = dewRcon.Port.ToString();
            tB_ServerProtocol.Text = dewRcon.Protocol;

            comB_ServerList.Text = tB_ServerAddress.Text;
        }

        private void btn_ResetSettings_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Reset();

            tB_ServerAddress.Text = dewRcon.Address;
            tB_ServerPort.Text = dewRcon.Port.ToString();
            tB_ServerProtocol.Text = dewRcon.Protocol;
        }

        private int sortColumn = -1;

        private void lV_Players_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column != sortColumn)
            {
                sortColumn = e.Column;
                lV_PlayersRed.Sorting = SortOrder.Ascending;
            }
            else
            {
                if (lV_PlayersRed.Sorting == SortOrder.Ascending)
                    lV_PlayersRed.Sorting = SortOrder.Descending;
                else
                    lV_PlayersRed.Sorting = SortOrder.Ascending;
            }

            lV_PlayersRed.Sort();
            lV_PlayersRed.ListViewItemSorter = new Helper.ListViewItemComparer(e.Column, lV_PlayersRed.Sorting, colH_RedPlayerIPAddress.Index);
        }

        private void tMSMI_KickPlayer_Click(object sender, EventArgs e)
        {
            if (lV_PlayersRed.SelectedIndices[0] >= 0)
                dewRcon.Send("Server.KickUid " + lV_PlayersRed.Items[lV_PlayersRed.SelectedIndices[0]].SubItems[0].Text);
        }

        private void tMSMI_BanPlayer_Click(object sender, EventArgs e)
        {
            if (lV_PlayersRed.SelectedIndices[0] >= 0)
                dewRcon.Send("Server.KickBanUid " + lV_PlayersRed.Items[lV_PlayersRed.SelectedIndices[0]].SubItems[0].Text);
        }

        private string uniqueid { get; set; }
        private string ipadress { get; set; }
        private string name { get; set; }
        private string team { get; set; }
        private string score { get; set; }
        private string kills { get; set; }
        private string deaths { get; set; }
        private string assists { get; set; }

        public class Player
        {
            public class Data
            {
                public string UniqueID { get; set; }
                public string IPAddress { get; set; }
                public string Name { get; set; }
                public string Team { get; set; }
                public string Score { get; set; }
                public string Kills { get; set; }
                public string Deaths { get; set; }
                public string Assists { get; set; }
            }
        }

        public static List<string> Player_List = new List<string>();

        private void GetServerInfo()
        {
            GetPlayerList();
            Thread.Sleep(500);

            try
            {
                dynamic dewJson = new Helper().GetDewJson(dewRcon.Address);
                
                if (new Helper().isNull(dewJson["passworded"]) == true)
                {
                    Invoke(new Action(() => Text = "Dewricon: " + dewJson["name"]));
                    tB_HostName.Invoke(new Action(() => tB_HostName.Text = dewJson["hostPlayer"]));
                    tB_VoIPEnabled.Invoke(new Action(() => tB_VoIPEnabled.Text = dewJson["VoIP"]));
                    tB_MapName.Invoke(new Action(() => tB_MapName.Text = dewJson["map"]));
                    tB_GameType.Invoke(new Action(() =>
                    {
                        if (dewJson["variant"] != null)
                            tB_GameType.Text = dewJson["variant"];
                        else
                            tB_GameType.Text = dewJson["variantType"];
                    }));
                    tB_NumPlayers.Invoke(new Action(() => tB_NumPlayers.Text = dewJson["numPlayers"]));
                    tB_MaxPlayers.Invoke(new Action(() => tB_MaxPlayers.Text = dewJson["maxPlayers"]));
                    
                    object _n, _v, _s, _u, _a, _m;

                    if (new Helper().isNull(dewJson["name"])) _n = "NULLNAME";
                    else _n = dewJson["name"];
                    if (new Helper().isNull(dewJson["VoIP"])) _v = 0;
                    else if (BoolParser.GetValue(dewJson["VoIP"].ToString())) _v = 1; else _v = 0;
                    if (new Helper().isNull(dewJson["sprintEnabled"])) _s = 0;
                    else if (BoolParser.GetValue(dewJson["sprintEnabled"].ToString())) _s = 1; else _s = 0;
                    if (new Helper().isNull(dewJson["sprintUnlimitedEnabled"])) _u = 0;
                    else if (BoolParser.GetValue(dewJson["sprintUnlimitedEnabled"].ToString())) _u = 1; else _u = 0;
                    if (new Helper().isNull(dewJson["assassinationEnabled"])) _a = 0;
                    else if (BoolParser.GetValue(dewJson["assassinationEnabled"].ToString())) _a = 1; else _a = 0;
                    if (new Helper().isNull(dewJson["maxPlayers"])) _m = 0;
                    else _m = dewJson["maxPlayers"].ToString();

                    new SettingsForm().GetSettings(_n.ToString(), Int32.Parse(_v.ToString()), Int32.Parse(_s.ToString()), Int32.Parse(_u.ToString()), Int32.Parse(_a.ToString()), Int32.Parse(_m.ToString()));

                    List<Player.Data> playerItems = new List<Player.Data>();

                    foreach (var playerData in dewJson["players"])
                    {
                        try
                        {
                            name = playerData["name"];

                            if (!dewRcon.isConnected)
                                uniqueid = playerData["uid"];
                            else
                            {
                                if (playerData["uid"] == 0000000000000000)
                                    PlayerList.ForEach(Player =>
                                    {
                                        if (Player.Name == name)
                                            uniqueid = Player.UniqueID;
                                    });
                            }
                            
                            if (new Helper().isNull(dewJson["teams"]) == false)
                            {
                                if (!(bool)dewJson["teams"])
                                {
                                    ColumnShow(colH_RedPlayerTeam, lV_PlayersRed, false);
                                    ColumnShow(colH_BluePlayerTeam, lV_PlayersBlue, false);
                                }
                                else
                                {
                                    ColumnShow(colH_RedPlayerTeam, lV_PlayersRed);
                                    ColumnShow(colH_BluePlayerTeam, lV_PlayersBlue);

                                    if ((int)playerData["team"] == 0)
                                        team = "Red";
                                    else if ((int)playerData["team"] == 1)
                                        team = "Blue";
                                }
                            }
                            else
                            {
                                ColumnShow(colH_RedPlayerTeam, lV_PlayersRed, false);
                                ColumnShow(colH_BluePlayerTeam, lV_PlayersBlue, false);
                            }

                            if ((int)playerData["score"] >= 0 && (int)playerData["score"] <= 9) score = playerData["score"].ToString("D2");
                            else score = playerData["score"];
                            if ((int)playerData["kills"] >= 0 && (int)playerData["kills"] <= 9) kills = playerData["kills"].ToString("D2");
                            else kills = playerData["kills"];
                            if ((int)playerData["deaths"] >= 0 && (int)playerData["deaths"] <= 9) deaths = playerData["deaths"].ToString("D2");
                            else deaths = playerData["deaths"];
                            if ((int)playerData["assists"] >= 0 && (int)playerData["assists"] <= 9) assists = playerData["assists"].ToString("D2");
                            else assists = playerData["assists"];
                            
                            playerItems.Add(new Player.Data()
                            {
                                UniqueID = uniqueid,
                                IPAddress = ipadress,
                                Name = name,
                                Team = team,
                                Score = score,
                                Kills = kills,
                                Deaths = deaths,
                                Assists = assists
                            });
                            Player_List.Add(name.ToString());
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                            MessageBox.Show(ex.StackTrace);
                        }
                    }
                    PlayersToListView("Red", lV_PlayersRed, playerItems);
                    PlayersToListView("Blue", lV_PlayersBlue, playerItems);
                    
                }
                else
                    MessageBox.Show("Server is password protected, please choose another.");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MessageBox.Show(ex.StackTrace);
            }
        }

        public void PlayersToListView(String Team, ListView TeamListView, List<Player.Data> PlayerDataList)
        {
            List<ListViewItem> lV_PlayersItems = new List<ListViewItem>();
            TeamListView.Invoke(new Action(() =>
            {
                for (int i = 0; i < PlayerDataList.Count; i++)
                {
                    if (PlayerDataList[i].Team == Team)
                    {
                        PlayerList.ForEach(Player => {
                            if (PlayerDataList[i].Name == Player.Name && PlayerDataList[i].IPAddress == null)
                                PlayerDataList[i].IPAddress = Player.IPAddress;
                        });

                        string[] PlayerInfo = {
                                    PlayerDataList[i].UniqueID,
                                    PlayerDataList[i].IPAddress,
                                    PlayerDataList[i].Name,
                                    PlayerDataList[i].Team,
                                    PlayerDataList[i].Score,
                                    PlayerDataList[i].Kills,
                                    PlayerDataList[i].Deaths,
                                    PlayerDataList[i].Assists
                                };

                        ListViewItem LViewItems = new ListViewItem(PlayerInfo);
                        lV_PlayersItems.Add(LViewItems);

                    }
                }
                TeamListView.Items.Clear();
                TeamListView.Items.AddRange(lV_PlayersItems.ToArray());
            }));
        }

        List<dynamic> PlayerList = new List<dynamic>();

        public void GetPlayerList()
        {
            dewRcon.ConsoleOutput = dewRcon.HiddenOutput;
            dewRcon.Log = dewRcon.HiddenOutput;

            PlayerList.Clear();

            if (dewRcon.isConnected)
            {
                dewRcon.Send("Server.ListPlayers");

                if (dewRcon.lastCommand == "Server.ListPlayers")
                    while (String.IsNullOrEmpty(dewRcon.lastMessage) || !dewRcon.lastMessage.StartsWith("["))
                    {
                        dewRcon.Send("Server.ListPlayers");
                        Thread.Sleep(500);
                    }

                string[] Players = new Helper().GetRconMessage("Server.ListPlayers").Split('\n');

                if (Players[0].StartsWith("["))
                {
                    foreach (string Player in Players)
                        if (String.IsNullOrEmpty(Player) == false)
                            PlayerList.Add(new
                            {
                                Name = Regex.Match(Regex.Match(Player, "\"[a-zA-Z0-9 .]+\"").Value, @"[a-zA-Z .]+").Value,
                                Index = Regex.Match(Regex.Match(Player, @"\[\d+\]").Value, @"\d+").Value,
                                UniqueID = Regex.Match(Regex.Match(Player, @"uid: [A-Za-z0-9]+").Value, @"[A-Za-z0-9]+").NextMatch().Value,
                                IPAddress = Regex.Match(Regex.Match(Player, @"ip: [0-9.0-9.0-9.0-9]+").Value, @"[0-9.0-9.0-9.0-9]+").Value
                            });
                    
                    PlayerList.ForEach(Player =>
                    {
                        dewRcon.ConsoleOutput.Invoke(new Action(() => dewRcon.ConsoleOutput.AppendText(
                            String.Format("{0}: [{1}] ({2}) {3}", Player.Name, Player.Index, Player.UniqueID, Player.IPAddress) + Environment.NewLine
                            )));
                    });
                }

                ColumnShow(colH_RedPlayerIPAddress, lV_PlayersRed);
                ColumnShow(colH_BluePlayerIPAddress, lV_PlayersBlue);
            }
            else
            {
                ColumnShow(colH_RedPlayerIPAddress, lV_PlayersRed, false);
                ColumnShow(colH_BluePlayerIPAddress, lV_PlayersBlue, false);
            }

            rTB_HiddenOutput.Invoke(new Action(() => rTB_HiddenOutput.Clear()));
            dewRcon.ConsoleOutput = rTB_ConsoleOutput;
            dewRcon.Log = rTB_ConsoleOutput;
        }

        private void comB_ServerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            comB_ServerList.Invoke(new Action(() => Properties.Settings.Default.ServerAddress = comB_ServerList.SelectedItem.ToString()));
            Properties.Settings.Default.Save();
            //MessageBox.Show(comB_ServerList.SelectedItem.ToString());
        }

        public void GetServerList()
        {
            List<string> Servers = new List<string>(new Helper().GetServersJson("158.69.166.144:8080"));
            Servers.Sort();

            foreach (dynamic server in Servers)
            {
                comB_ServerList.Invoke(new Action(() => comB_ServerList.Items.Add(Text = server)));
            }
        }

        private void ColumnShow(ColumnHeader Column, ListView Header, bool action = true)
        {
            if (action == false)
            {
                Header.Invoke(new Action(() => {
                    if (Column.Width != 0)
                    {
                        Column.Tag = Column.Width;
                        Column.Width = 0;
                    }
                }));
            }
            else
            {
                Header.Invoke(new Action(() => {
                    if (Column.Width == 0)
                    {
                        Column.Width = (int)Column.Tag;
                        Column.Tag = 0;
                    }
                }));
            }
        }
        #endregion
        #region Console Tab
        private void btn_ConsoleSend_Click(object sender, EventArgs e)
        {
            SendConsole();
        }

        private void tB_ConsoleInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SendConsole();
        }

        void SendConsole()
        {
            try
            {
                dewRcon.Send(tB_ConsoleInput.Text);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btn_ChangeTime_Click(object sender, EventArgs e)
        {
            if (new Helper().DateTimeUtc)
            {
                new Helper().DateTimeUtc = false;
                new Helper().DateTimeNow = DateTime.Now;
                btn_ChangeTime.Text = "Universal Time";
                btn_ChangeTime.Tag = "Local Time";
            }
            else
            {
                new Helper().DateTimeUtc = true;
                new Helper().DateTimeNow = DateTime.UtcNow;
                btn_ChangeTime.Text = "Local Time";
                btn_ChangeTime.Tag = "Universal Time";
            }

            string logFormatted = String.Format("[{1}]: Time Changed: {2}{0}", Environment.NewLine, new Helper().DateTimeNow, btn_ChangeTime.Tag);
            dewRcon.ConsoleOutput.Invoke(new Action(() => dewRcon.ConsoleOutput.AppendText(logFormatted)));
        }
        #endregion
        #region Workers
        private void bgW_GetServerInfo_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            if (nUD_RefreshInterval.Value == 0)
                GetServerInfo();
            else
                while (nUD_RefreshInterval.Value >= 1)
                {
                    GetServerInfo();
                    Thread.Sleep(1000 * (int)nUD_RefreshInterval.Value);
                }
        }

        private void bgW_GetPlayerList_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            GetPlayerList();
        }
        #endregion
    }
}